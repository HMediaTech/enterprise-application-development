
var express = require('express')
var app = express();

var Sequelize = require('sequelize');
var sequelize = new Sequelize('postgres://ali:aliko1993@localhost:5432/lab2');

const models = require("./models");

var bodyParser = require('body-parser');

var bcrypt = require('bcrypt');

var jwt = require('jsonwebtoken');

var crypto = require('generate-key');

app.use(bodyParser.urlencoded({extended: true}));

var user_token ='';

// create tables
// users, and products
models.sequelize.sync()
	.then(() => models.Users.destroy({
		where: {}
  }))

models.sequelize.sync()
	.then(() => models.Products.destroy({
		where: {}
  }))

// Populate models
models.sequelize.sync()
	.then(() => models.Users.bulkCreate([
		{name: 'Ali', password: bcrypt.hashSync("Dublin", 10), access: crypto.generateKey(16), secret: crypto.generateKey(32)},
		{name: 'John', password: bcrypt.hashSync("HelloWorld", 10), access:crypto.generateKey(16), secret: crypto.generateKey(32)}
	]))

models.sequelize.sync()
	.then(() => models.Products.bulkCreate([
		{name: 'product1', provider:'Google'},
		{name: 'product2', provider:'Microsoft'}
	]))


app.get('/users', function(req,res){
	models.Users.findAll({}).then(function(d){
		res.json(d);
	});
});

app.get('/products', function(req,res){
	if(tokenize(user_token)){
		// if token is not expired
		models.Products.findAll({}).then(function(d){
			res.json(d);
		});
    }
    else{
    	res.status(400).send({success: false,
			                      message: "Authentication failed, Please login again!"});
    }
});

app.post("/login", function(req, res) {
   models.Users.findOne({
  	where: {name: req.body.username},
  }).then(function(d){
  	    // id return data is null
		if(d == null){
			res.status(400).send({success: false,
			                      message: "Authentication failed. Incorrect username"});
		}else{
			if(bcrypt.compareSync(req.body.password, d.password)){

			  var load ={
			  	uname: req.body.name
			  };
              
              // create token with username
			  var token = jwt.sign(load, 'tokenKey', {
			  	expiresIn: 60*60*24
			  });

			  res.status(200).send({success: true,
			                      message: "Successfully found!",
			                      token: token,
			                      access:d.access,
			                      secret:d.secret
			                      });
			  // save token 
			  user_token = token;
		      }
		    else{
		    	res.status(400).send({success: false,
			                      message: "Authentication failed. Incorrect password"});
		    	}
		}
  });
});

// check if the token is valid or not
var tokenize = function(token){
	if(token!=null){
		try{
			jwt.verify(token, 'tokenKey')
			return true;
		}
		catch(err){
			return false;
		}
	}
};

app.listen(3000, function () {
  console.log('Example app listening on port 3000!')
})	 