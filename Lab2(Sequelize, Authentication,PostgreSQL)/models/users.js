'use strict';
const bcrypt = require("bcrypt");

module.exports = (sequelize, DataTypes) => {
  var Users = sequelize.define('Users', {
    name: DataTypes.STRING,
    password: DataTypes.STRING,
    access: DataTypes.STRING,
    secret: DataTypes.STRING
  }, {});

  Users.associate = function(models) {
    // associations can be defined here
  };

  return Users;
};