
// Important note: Massive version 3 library doesn't support raw sql queries. 
// For this functionalities Massive 2 library imported 

var express = require('express');
var graphqlHTTP = require('express-graphql');
var { buildSchema } = require('graphql');

var products = []

var app = express();

var Massive=require("massive");

// connect to the postgres with connectSyncmethod
var db = Massive.connectSync({
  connectionString: 'postgres://ali:aliko1993@localhost:5432/dellstore2'
});


// get the data from postgres using massive, and set products to 
app.get('/', function (req, res) {
  db.run(`select * from postgraphile.products`, function(err, data){
    products = data;
    res.send(products);
  })
})

var schema = buildSchema(`
  type Query {
    category(id: Int!):String,
  }
`);

var root = { category:({id}) => { 
				 	for (var i = 0; i < products.length; i++){
				 		if(id === products[i].prod_id){
					       return products[i].category} 
					   }
				    }
};

app.use('/graphql', graphqlHTTP({
  schema: schema,
  rootValue: root,
  graphiql: true,
}));


app.listen(3000, function () {
  console.log('Example app listening on port 3000!')
})